# \[ 🚧 Work in progress 👷‍♀️⛏👷🔧️👷🔧 🚧 \] Employee Portal

EmployeePortal is a project to showcase "What's New In Android".

Kotlin Coroutines for background operations.
A single-activity architecture, using the Navigation component to manage fragment operations.
A presentation layer that contains a fragment (View) and a ViewModel per screen (or feature).
Reactive UIs using LiveData observables and Flow.

## Useful Links
- [MDC-Android on Stack
  Overflow](https://www.stackoverflow.com/questions/tagged/material-components+android)
  (external site)
- [Android Developer’s
  Guide](https://developer.android.com/training/material/index.html)
  (external site)
- [Material.io](https://www.material.io) (external site)
- [Material Design Guidelines](https://material.google.com) (external site)